package kaj.kajssnackbar.fragment;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

public class SnackFragment extends ItemListFragment {

    public SnackFragment() {
    }

    @Override
    public Query getQuery(DatabaseReference databaseReference) {
        return databaseReference.child("snack").orderByKey();
    }
}
