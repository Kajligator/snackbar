package kaj.kajssnackbar.fragment;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

public class FrietFragment extends ItemListFragment {

    public FrietFragment() {
    }

    @Override
    public Query getQuery(DatabaseReference databaseReference) {
        return databaseReference.child("friet").orderByKey();
    }
}
