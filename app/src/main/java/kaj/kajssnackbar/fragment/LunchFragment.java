package kaj.kajssnackbar.fragment;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

public class LunchFragment extends ItemListFragment {

    public LunchFragment() {
    }

    @Override
    public Query getQuery(DatabaseReference databaseReference) {
        return databaseReference.child("lunch").orderByKey();
    }
}
