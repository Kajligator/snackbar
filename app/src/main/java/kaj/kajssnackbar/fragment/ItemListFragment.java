package kaj.kajssnackbar.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;

import kaj.kajssnackbar.MenuActivity;
import kaj.kajssnackbar.R;
import kaj.kajssnackbar.models.Item;
import kaj.kajssnackbar.viewholder.ItemViewHolder;


public abstract class ItemListFragment extends Fragment {

    private static final String TAG = "ItemListFragment";
    public String tabName;

    private DatabaseReference mDatabase;
    private FirebaseRecyclerAdapter<Item, ItemViewHolder> mAdapter;
    private RecyclerView mRecycler;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_all_items, container, false);

        // [START create_database_reference]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END create_database_reference]

        mRecycler = (RecyclerView) rootView.findViewById(R.id.item_list);
        mRecycler.setHasFixedSize(true);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Set up Layout Manager, reverse layout
        LinearLayoutManager mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(false);
        mManager.setStackFromEnd(false);
        mRecycler.setLayoutManager(mManager);

        // Set up FirebaseRecyclerAdapter with the Query
        Query postsQuery = getQuery(mDatabase);
        mAdapter = new FirebaseRecyclerAdapter<Item, ItemViewHolder>(Item.class, R.layout.list_item,
                ItemViewHolder.class, postsQuery) {

            @Override
            protected void populateViewHolder(final ItemViewHolder viewHolder, final Item model, final int position) {
                final DatabaseReference postRef = getRef(position);

                // Bind Item to ViewHolder, setting OnClickListener for the add and subtract button
                viewHolder.bindToItem(model, new View.OnClickListener() {

                    @Override
                    public void onClick(View addButton) {
                        DatabaseReference globalPostRef = mDatabase.child(getTabPosition()).child(postRef.getKey());
                        onAddClicked(globalPostRef);
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View subtractButton) {
                        DatabaseReference globalPostRef = mDatabase.child(getTabPosition()).child(postRef.getKey());
                        onSubtractClicked(globalPostRef);
                    }
                });
            }
        };
        mRecycler.setAdapter(mAdapter);
    }

    private void onAddClicked(DatabaseReference postRef) {
        Log.d("onAddClicked", "jeeej for the add");
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Item i = mutableData.getValue(Item.class);
                if (i == null) {
                    return Transaction.success(mutableData);
                }
                // Add on item
                i.itemQuantity = i.itemQuantity + 1;
                // Set value and report transaction success
                mutableData.setValue(i);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }
        });
    }

    private void onSubtractClicked(DatabaseReference postRef) {
        Log.d("onSubtractClicked", "jeeej for the subtract");
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Item i = mutableData.getValue(Item.class);
                if (i == null) {
                    return Transaction.success(mutableData);
                }
                // subtract one item
                i.itemQuantity = i.itemQuantity - 1;
                // Set value and report transaction success
                mutableData.setValue(i);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    /*    public String getUid() {
            return FirebaseAuth.getInstance().getCurrentUser().getUid();
        }
    */
    public abstract Query getQuery(DatabaseReference databaseReference);

    public String getTabPosition() {
        if (MenuActivity.tabPosition == 0) {
            tabName = "snack";
            Log.d(TAG, "setting tab to: " + tabName);
        } else if (MenuActivity.tabPosition == 1) {
            tabName = "lunch";
            Log.d(TAG, "setting tab to: " + tabName);
        }
        return tabName;
    }


}

