package kaj.kajssnackbar.auth;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import kaj.kajssnackbar.OverviewActivity;
import kaj.kajssnackbar.R;
import kaj.kajssnackbar.models.User;
import kaj.kajssnackbar.utils.TrackUser;

import static kaj.kajssnackbar.R.id.but_login_google;
import static kaj.kajssnackbar.R.id.but_logout;

public class AuthMenu extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final String TAG = "AuthMenu";
    private static final int RC_SIGN_IN = 9001;

    private static Boolean skipLogin = true;

    TrackUser trackUser;

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private SignInButton signInButton;
    private Button signOutButton;
    private TextView statusTextView;
    private GoogleApiClient mGoogleApiClient;

    public static void skipLogin(boolean b) {
        skipLogin = b;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        trackUser = new TrackUser();

        statusTextView = (TextView) findViewById(R.id.statusView);

        signInButton = (SignInButton) findViewById(but_login_google);
        signInButton.setOnClickListener(this);

        signOutButton = (Button) findViewById(but_logout);
        signOutButton.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (mAuth.getCurrentUser() != null) {
            if (skipLogin) {
                onAuthSuccess(mAuth.getCurrentUser());
                updateUI(currentUser);
            }
        } else {
            updateUI(null);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.but_login_google:
                signIn();
                break;
            case R.id.but_logout:
                signOut();
                break;
        }
    }

    private void signOut() {
        // Firebase sign out
        mAuth.signOut();

        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                Log.d(TAG, "User Signed out");
                updateUI(null);
            }
        });
    }


    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                Log.d(TAG, "Google Sign In failed, update UI appropriately");

                updateUI(null);

            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            onAuthSuccess(user);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(AuthMenu.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                    }
                });
    }


    private void onAuthSuccess(FirebaseUser user) {
        // Write new user
        writeNewUser(user.getUid(), user.getDisplayName(), user.getEmail(), user.getPhotoUrl());
        //updateUserInformation( user.getDisplayName(), user.getEmail());
        updateUI(user);
        Toast.makeText(AuthMenu.this, "Authentication successful",
                Toast.LENGTH_SHORT).show();

        // Go to OverviewActivity
        startActivity(new Intent(AuthMenu.this, OverviewActivity.class));
        finish();
    }

    private void writeNewUser(String userId, String name, String email, Uri photoUrl) {
        User user = new User(name, email, photoUrl);
        mDatabase.child("users").child(userId).setValue(user);

        trackUser.setName(name);
        trackUser.setEmailAddress(email);
        trackUser.setPhotoUrl(photoUrl);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            statusTextView.setText(getString(R.string.google_status_fmt, user.getUid()));
            signOutButton.setEnabled(true);
            signInButton.setEnabled(false);
        } else {
            statusTextView.setText(R.string.signed_out);
            signOutButton.setEnabled(false);
            signInButton.setEnabled(true);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }


}