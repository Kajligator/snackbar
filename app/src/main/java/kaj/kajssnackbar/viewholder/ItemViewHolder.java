package kaj.kajssnackbar.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import kaj.kajssnackbar.R;
import kaj.kajssnackbar.models.Item;

/**
 * Created by Kaj van der Ster on 1-4-2017.
 */

public class ItemViewHolder extends RecyclerView.ViewHolder {

    public TextView titleView;
    public TextView priceView;
    public TextView quantityView;
    public Button addButton;
    public Button subtractButton;

    public ItemViewHolder(View itemView) {
        super(itemView);

        titleView = (TextView) itemView.findViewById(R.id.item_title);
        priceView = (TextView) itemView.findViewById(R.id.item_price);
        quantityView = (TextView) itemView.findViewById(R.id.item_quantity);
        addButton = (Button) itemView.findViewById(R.id.buttonAdd);
        subtractButton = (Button) itemView.findViewById(R.id.buttonSubtract);


    }

    public void bindToItem(Item item, View.OnClickListener addClickListener, View.OnClickListener subtractClickListener) {
        titleView.setText(item.itemName);
        priceView.setText(item.itemPrice);
        quantityView.setText(Objects.toString(item.itemQuantity));

        addButton.setOnClickListener(addClickListener);
        subtractButton.setOnClickListener(subtractClickListener);

    }
}

