package kaj.kajssnackbar.models;

import com.google.firebase.database.IgnoreExtraProperties;

// [START item_class]
@IgnoreExtraProperties
public class Item {

    public String itemName;
    public String itemPrice;
    public Long itemQuantity;

    // Required default constructor for Firebase object mapping
    @SuppressWarnings("unused")
    public Item() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public Item(String itemName, Long itemQuantity, String itemPrice) {
        this.itemName = itemName;
        this.itemQuantity = itemQuantity;
        this.itemPrice = itemPrice;
    }
}
// [END item_class]