package kaj.kajssnackbar.models;

import android.net.Uri;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {

    public String username;
    public String email;
    public String avatarURL;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String username, String email, Uri avatarURL) {
        this.username = username;
        this.email = email;
        this.avatarURL = avatarURL.toString();
    }
}
