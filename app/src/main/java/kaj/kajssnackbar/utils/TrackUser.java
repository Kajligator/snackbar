package kaj.kajssnackbar.utils;

import android.net.Uri;

/**
 * Created by Kaj van der Ster on 10-7-2017.
 */

public class TrackUser {

    private static String name;
    private static String emailAddress;
    private static String photoUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(Uri photoUrl) {
        this.photoUrl = photoUrl.toString();
    }
}
