package kaj.kajssnackbar.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;


import java.io.InputStream;

/**
 * Created by Kaj van der Ster on 10-7-2017.
 */

public class UrlDownloader extends AsyncTask<String, Void, Bitmap> {
    private ImageView bmImage;
    private String TAG = "UrlDownloader";

    /**
     *
     * @param bmImage
     */
    public UrlDownloader(ImageView bmImage) {
        this.bmImage = bmImage;
    }

    /**
     *
     * @param urls
     * @return
     */
    protected Bitmap doInBackground(String... urls) {
        String urlDisplay = urls[0];
        Log.d(TAG, urlDisplay);
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urlDisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    /**
     *
     * @param result
     */
    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(GetBitmapClippedCircle(result));
    }

    /**
     *
     * @param bitmap
     * @return
     */
    private static Bitmap GetBitmapClippedCircle(Bitmap bitmap) {

        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();
        final Bitmap outputBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        final Path path = new Path();
        path.addCircle(
                (float)(width / 2)
                , (float)(height / 2)
                , (float) Math.min(width, (height / 2))
                , Path.Direction.CCW);

        final Canvas canvas = new Canvas(outputBitmap);
        canvas.clipPath(path);
        canvas.drawBitmap(bitmap, 0, 0, null);
        return outputBitmap;
    }
}